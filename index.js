const express = require('express');
const bodyParser = require('body-parser');
const answers = require('./Answers');



const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', (req, res) => {
    res.sendFile('./video.html', { root: __dirname });
});

app.get('/answers', (req, res) => {
    res.json(answers);
    // console.log("posted data",req);
    
});

app.post('/answers', (req, res) => {
    // you have address available in req.body:
    console.log("body",req.body);
    res.status(200)
    const newAnswers = {
        'answer1': req.body.answer1,
        'answer2' : req.body.answer2
    }
    answers.push(newAnswers);
    
  });


app.listen(process.env.PORT || 3000, () => console.log("App is running on port 3000"));